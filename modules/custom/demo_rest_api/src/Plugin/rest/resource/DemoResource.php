<?php
namespace Drupal\demo_rest_api\Plugin\rest\resource;

use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Drupal\Core\Database\Database;

/**
 * Provides a Demo Resource
 *
 * @RestResource(
 *   id = "demo_resource",
 *   label = @Translation("Demo Resource"),
 *   uri_paths = {
 *     "canonical" = "/demo_rest_api/demo_resource"
 *   }
 * )
 */

class DemoResource extends ResourceBase {
	/**
   * Responds to entity GET requests.
   * @return \Drupal\rest\ResourceResponse
   */
  public function get() {
  	

   $name   = $_GET['name'];
   $email  = $_GET['email'];
   $mobile = $_GET['mobile'];
   $dob    = $_GET['dob'];
   $gender = $_GET['gender'];
   $confirmation = $_GET['confirmation'];
   $copy = $_GET['copy'];
   

   $conn = Database::getConnection();
   $conn->insert('tbl_userprofile')->fields(
		  array(
		    'name' => $name,
		    'email' => $email,
		    'mobile' => $mobile,
		    'dob' => $dob,
		    'gender' => $gender,
		    'confirmation' => $confirmation,
		    'copy' => $copy,
		  )
		)->execute();

    $response = [ 'name' => $name,
		    'email' => $email,
		    'mobile' => $mobile,
		    'dob' => $dob,
		    'gender' => $gender,
		    'confirmation' => $confirmation,
		    'copy' => $copy,'message' => 'Hello, your are successfully registered...'];

    return new ResourceResponse($response);
  }


}
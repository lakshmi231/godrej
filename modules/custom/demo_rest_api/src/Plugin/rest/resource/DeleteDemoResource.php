<?php
namespace Drupal\demo_rest_api\Plugin\rest\resource;

use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Drupal\Core\Database\Database;

/**
 * Provides a Demo Resource
 *
 * @RestResource(
 *   id = "delete_demo_resource",
 *   label = @Translation("Delete Demo Resource"),
 *   uri_paths = {
 *     "canonical" = "/demo_rest_api/delete_demo_resource"
 *   }
 * )
 */

class DeleteDemoResource extends ResourceBase {
  
  /**
   * Responds to entity GET requests.
   * @return \Drupal\rest\ResourceResponse
   */
  public function get() {
    
   // print_r($data);die; 
   $id   = $_GET['id'];
   // print_r($id);die;


$field =   array(
		    'name' => $name,
		    'email' => $email,
		    'mobile' => $mobile,
		    'dob' => $dob,
		    'gender' => $gender,
		    'confirmation' => $confirmation,
		    'copy' => $copy,
		  );


   $query = \Drupal::database();
          $query->delete('tbl_userprofile')
              //->fields($field)
              ->condition('id', $id)
              ->execute();

    $response = ['message' => 'Hello, your are successfully Deleted...'];

    return new ResourceResponse($response);
  }


}
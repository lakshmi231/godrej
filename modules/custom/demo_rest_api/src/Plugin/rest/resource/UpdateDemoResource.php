<?php
namespace Drupal\demo_rest_api\Plugin\rest\resource;

use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Drupal\Core\Database\Database;

/**
 * Provides a Demo Resource
 *
 * @RestResource(
 *   id = "update_demo_resource",
 *   label = @Translation("Update Demo Resource"),
 *   uri_paths = {
 *     "canonical" = "/demo_rest_api/update_demo_resource"
 *   }
 * )
 */

class UpdateDemoResource extends ResourceBase {
  
  /**
   * Responds to entity GET requests.
   * @return \Drupal\rest\ResourceResponse
   */
  public function get() {

   $id   = $_GET['id'];
   $name = $_GET['name'];
   $email  = $_GET['email'];
   $mobile = $_GET['mobile'];
   $dob    = $_GET['dob'];
   $gender = $_GET['gender'];
   $confirmation = $_GET['confirmation'];
   $copy = $_GET['copy'];


$field =   array(
		    'name' => $name,
		    'email' => $email,
		    'mobile' => $mobile,
		    'dob' => $dob,
		    'gender' => $gender,
		    'confirmation' => $confirmation,
		    'copy' => $copy,
		  );


   $query = \Drupal::database();
          $query->update('tbl_userprofile')
              ->fields($field)
              ->condition('id', $id)
              ->execute();

    $response = ['message' => 'Hello, your are successfully updated...'];

    return new ResourceResponse($response);
  }


}
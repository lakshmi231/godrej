<?php
namespace Drupal\resume\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
/**
 * An example controller.
 */
class ResumeController extends ControllerBase {

  /**
   * Returns a render-able array for a test page.
   */
  public function content()
  {

    $node = \Drupal\node\Entity\Node::create(['type' => 'schedule_a_visite']); 
    $form = \Drupal::service('entity.form_builder')->getForm($node);
    return $form;
  }

}
?>
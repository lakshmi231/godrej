<?php
namespace Drupal\laxmi_routing\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * An example controller.
 */
class LaxmiController extends ControllerBase {

  /**
   * Returns a render-able array for a test page.
   */
  public function content() {
    $build = [
      '#markup' => $this->t('Hello World!'),
    ];
    return $build;
  }

  public function index()
  {

  	$build = [
      '#markup' => $this->t('Test for listing'),
    ];
    return $build;
  }

}


?>
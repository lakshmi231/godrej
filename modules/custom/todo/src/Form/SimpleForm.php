<?php
/**
 * 
 */
namespace Drupal\todo\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Database;
class SimpleForm extends FormBase
{
	
	public function getFormId()
	{
		return 'simple_form';
	}

	 /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    \Drupal::logger('todo')->info('Info');
  	//echo $_GET['num'];die;
    $form['candidate_name'] = array(
      '#type' => 'textfield',
      '#title' => t('Candidate Name:'),
      '#required' => TRUE,
    );
    $form['candidate_mail'] = array(
      '#type' => 'email',
      '#title' => t('Email ID:'),
      '#required' => TRUE,
    );
    $form['candidate_number'] = array (
      '#type' => 'tel',
      '#title' => t('Mobile no'),
    );
    $form['candidate_dob'] = array (
      '#type' => 'date',
      '#title' => t('DOB'),
      '#required' => TRUE,
    );
    $form['candidate_gender'] = array (
      '#type' => 'select',
      '#title' => ('Gender'),
      '#options' => array(
        'Female' => t('Female'),
        'male' => t('Male'),
      ),
    );
    $form['candidate_confirmation'] = array (
      '#type' => 'radios',
      '#title' => ('Are you above 18 years old?'),
      '#options' => array(
        'Yes' =>t('Yes'),
        'No' =>t('No')
      ),
    );
    $form['candidate_copy'] = array(
      '#type' => 'checkbox',
      '#title' => t('Send me a copy of the application.'),
    );
    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
    );
    return $form;
  }


/**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {


    $conn = Database::getConnection();
	$conn->insert('tbl_userprofile')->fields(
	  array(
	    'name' => $form_state->getValue('candidate_name'),
	    'email' => $form_state->getValue('candidate_mail'),
	    'mobile' => $form_state->getValue('candidate_number'),
	    'dob' => $form_state->getValue('candidate_dob'),
	    'gender' => $form_state->getValue('candidate_gender'),
	    'confirmation' => $form_state->getValue('candidate_confirmation'),
	    'copy' => $form_state->getValue('candidate_copy'),
	    'form_build_id' => $form_state->getValue('form_build_id'),
	  )
	)->execute();
          drupal_set_message("succesfully updated");
          $form_state->setRedirect('todo.simple_form2');


   // drupal_set_message($this->t('@can_name ,Your application is being submitted!', array('@can_name' => $form_state->getValue('candidate_name'))));
    // foreach ($form_state->getValues() as $key => $value) {
    //   drupal_set_message($key . ': ' . $value);
    // }
   }



 

}
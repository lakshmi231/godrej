<?php
/**
 * 
 */
namespace Drupal\todo\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Database;
use Symfony\Component\HttpFoundation\RedirectResponse;

class SimpleFormupdate extends FormBase
{
	
	public function getFormId()
	{
		return 'simple_form_update';
	}

	 /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
  	// echo $_GET['num'];die;
    $conn = Database::getConnection();
     $record = array();
    if (isset($_GET['num'])) {
        $query = $conn->select('tbl_userprofile', 'm')
            ->condition('id', $_GET['num'])
            ->fields('m');
        $record = $query->execute()->fetchAssoc();
    }
    // echo "<pre>";
    // print_r($record);die;

    $form['candidate_name'] = array(
      '#type' => 'textfield',
      '#title' => t('Candidate Name:'),
      '#required' => TRUE,
      '#default_value' => (isset($record['name']) && $_GET['num']) ? $record['name']:'',
    );
    $form['candidate_mail'] = array(
      '#type' => 'email',
      '#title' => t('Email ID:'),
      '#required' => TRUE,
      '#default_value' => (isset($record['email']) && $_GET['num']) ? $record['email']:'',
    );
    $form['candidate_number'] = array (
      '#type' => 'tel',
      '#title' => t('Mobile no'),
      '#default_value' => (isset($record['mobile']) && $_GET['num']) ? $record['mobile']:'',
    );
    $form['candidate_dob'] = array (
      '#type' => 'date',
      '#title' => t('DOB'),
      '#required' => TRUE,
      '#default_value' => (isset($record['dob']) && $_GET['num']) ? $record['dob']:'',
    );
    $form['candidate_gender'] = array (
      '#type' => 'select',
      '#title' => ('Gender'),
      '#options' => array(
        'Female' => t('Female'),
        'male' => t('Male'),
      ),
      '#default_value' => (isset($record['gender']) && $_GET['num']) ? $record['gender']:'',
    );
    $form['candidate_confirmation'] = array (
      '#type' => 'radios',
      '#title' => ('Are you above 18 years old?'),
      '#options' => array(
        'Yes' =>t('Yes'),
        'No' =>t('No')
      ),
      '#default_value' => (isset($record['confirmation']) && $_GET['num']) ? $record['confirmation']:'',
    );
    $form['candidate_copy'] = array(
      '#type' => 'checkbox',
      '#title' => t('Send me a copy of the application.'),
      '#default_value' => (isset($record['copy']) && $_GET['num']) ? $record['copy']:'',
    );
    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
    );
    return $form;
  }


/**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  
  // echo "<pre>";
 // $id = $_POST['id'];

 $field = array(
      'name' => $form_state->getValue('candidate_name'),
      'email' => $form_state->getValue('candidate_mail'),
      'mobile' => $form_state->getValue('candidate_number'),
      'dob' => $form_state->getValue('candidate_dob'),
      'gender' => $form_state->getValue('candidate_gender'),
      'confirmation' => $form_state->getValue('candidate_confirmation'),
      'copy' => $form_state->getValue('candidate_copy'),
      'form_build_id' => $form_state->getValue('form_build_id'),
    );

 $query = \Drupal::database();
          $query->update('tbl_userprofile')
              ->fields($field)
              ->condition('id', $_GET['num'])
              ->execute();

              drupal_set_message("succesfully updated");
          $form_state->setRedirect('todo.simple_form2');

}



 

}
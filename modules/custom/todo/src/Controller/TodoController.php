<?php
namespace Drupal\todo\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
/**
 * An example controller.
 */
class TodoController extends ControllerBase {

  /**
   * Returns a render-able array for a test page.
   */
  public function content() {

/**
 *Simple example for how to works the Cache given commented
 */
  //   $a = 'Valuebound';

  //   if(\Drupal::cache()->get('mahaveer')) {
  //    print_r(\Drupal::cache()->get('mahaveer'));
  // exit;
  //   } else {
  //     \Drupal::cache()->set('mahaveer', $a);
  //    echo 'No cache';
  //  exit;
  //   }






$query = \Drupal::database()->select('tbl_userprofile', 'u');
$query->fields('u', ['id','name','email']);
$results = $query->execute()->fetchAll();

// echo "<pre>";
// print_r($results);die;
    if ($cache = \Drupal::cache()->get('laxmi')) {


  $cached_data = \Drupal::cache()->get('laxmi')->data;
  $form['text']['#markup'] = t('cached Data....');
  $header = [
       'id' => t('User id'),
       'name' => t('username'),
       'email' => t('Email'),
       'Edit' => t('Edit'),
       'Delete' => t('Delete'),
     ];

 // Initialize an empty array
$output = array();
foreach ($cached_data as $result) {

  $delete = Url::fromUserInput('/simple-delete/'.$result->id);
        $edit   = Url::fromUserInput('/simple-edit?num='.$result->id);

  

       $output[$result->id] = [
         'id' => $result->id,     
         'name' => $result->name, 
         'email' => $result->email,
         \Drupal::l('Edit', $edit),
         \Drupal::l('Delete', $delete),
      
       ];
   }
  $form['table'] = [
'#type' => 'table',
'#header' => $header,
'#rows' => $output,
'#empty' => t('No users found'),
];

return $form;
       

    }else {

       \Drupal::cache()->set('laxmi', $results);
$form['text']['#markup'] = t('<h1>cleared caching</h1>');
        $build = [
      '#markup' => $this->t('cached cleared!'),
    ];

$header = [
     'id' => t('User id'),
     'name' => t('username new'),
     'email' => t('Email'),
     'Edit' => t('Edit'),
     'Delete' => t('Delete'),
   ];

 // Initialize an empty array
$output = array();
foreach ($results as $result) {

  $delete = Url::fromUserInput('/simple-delete/'.$result->id);
        $edit   = Url::fromUserInput('/simple-edit?num='.$result->id);

  

       $output[$result->id] = [
         'id' => $result->id,     
         'name' => $result->name, 
         'email' => $result->email,
         \Drupal::l('Edit', $edit),
         \Drupal::l('Delete', $delete),
      
       ];
   }
  $form['table'] = [
'#type' => 'table',
'#header' => $header,
'#rows' => $output,
'#empty' => t('No users found'),
];

return $form;
    }
  
//     $build = [
//       '#markup' => $this->t('Hello World!'),
//     ];

// $query = \Drupal::database()->select('tbl_userprofile', 'u');
// $query->fields('u', ['id','name','email']);
// $results = $query->execute()->fetchAll();

// $header = [
//      'id' => t('User id'),
//      'name' => t('username'),
//      'email' => t('Email'),
//      'Edit' => t('Edit'),
//      'Delete' => t('Delete'),
//    ];

//  // Initialize an empty array
// $output = array();
// foreach ($results as $result) {

// 	$delete = Url::fromUserInput('/simple-delete/'.$result->id);
//         $edit   = Url::fromUserInput('/simple-edit?num='.$result->id);

	

//        $output[$result->id] = [
//          'id' => $result->id,     
//          'name' => $result->name, 
//          'email' => $result->email,
//          \Drupal::l('Edit', $edit),
//          \Drupal::l('Delete', $delete),
      
//        ];
//    }
//   $form['table'] = [
// '#type' => 'table',
// '#header' => $header,
// '#rows' => $output,
// '#empty' => t('No users found'),
// ];

// return $form;
 
 }

}
?>
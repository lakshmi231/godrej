<?php


namespace Drupal\manage_inventory\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Language\Language;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the manage_inventory entity edit forms.
 *
 * @ingroup manage_inventory
 */
class InventoryForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var $entity \Drupal\manage_inventory\Entity\Inventory */

    /***Commented by me start****/
    $form = parent::buildForm($form, $form_state);
    $entity = $this->entity;

    $form['langcode'] = array(
      '#title' => $this->t('Language'),
      '#type' => 'language_select',
      '#default_value' => $entity->getUntranslated()->language()->getId(),
      '#languages' => Language::STATE_ALL,
    );

    /***commented by me End***/ 

    // $form['candidate_name'] = array(
    //   '#type' => 'textfield',
    //   '#title' => t('Candidate Name:'),
    //   '#required' => TRUE,
    // );
    // $form['candidate_mail'] = array(
    //   '#type' => 'email',
    //   '#title' => t('Email ID:'),
    //   '#required' => TRUE,
      
    // );
    // $form['candidate_number'] = array (
    //   '#type' => 'tel',
    //   '#title' => t('Mobile no'),
    // );
    // $form['candidate_dob'] = array (
    //   '#type' => 'date',
    //   '#title' => t('DOB'),
    //   '#required' => TRUE,
    // );
    // $form['candidate_gender'] = array (
    //   '#type' => 'select',
    //   '#title' => ('Gender'),
    //   '#options' => array(
    //     'Female' => t('Female'),
    //     'male' => t('Male'),
    //   ),
      
    // );
    // $form['candidate_confirmation'] = array (
    //   '#type' => 'radios',
    //   '#title' => ('Are you above 18 years old?'),
    //   '#options' => array(
    //     'Yes' =>t('Yes'),
    //     'No' =>t('No')
    //   ),
      
    // );
    // $form['candidate_copy'] = array(
    //   '#type' => 'checkbox',
    //   '#title' => t('Send me a copy of the application.'),
    // );
    // $form['actions']['#type'] = 'actions';
    // $form['actions']['submit'] = array(
    //   '#type' => 'submit',
    //   '#value' => $this->t('Save'),
    //   '#button_type' => 'primary',
    // );

   // $form['candidate_name'] = array(
   //    '#type' => 'textfield',
   //    '#title' => t('Candidate Name:'),
   //    '#required' => TRUE,
   //  );

   //  $form['submit'] = array(
   //  '#type' => 'submit',
   //  '#value' => 'Submit',
   //  '#name' => 'employee-data',
   //  '#submit' => array('::submitEmployeeData')
    
  // );
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
 // print_r($_POST);die;
    $form_state->setRedirect('entity.content_entity_manage_inventory.collection');
    $entity = $this->getEntity();
    $entity->save();
  }

//   public function submitEmployeeData(array $form, FormStateInterface $form_state)
//   {
//     echo "<pre>";
// print_r($_POST);die;

//   }

}

?>